var express = require('Express');
var app = express();
var bodyParser = require('body-parser')

var index = require('./index');
var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(jsonParser)
//both index.js and things.js should be in same directory

app.use('/index', index);

app.listen(3000);