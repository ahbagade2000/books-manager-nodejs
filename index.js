const express = require('express');

const router = express.Router();

const db = require("./firestore");

let books = require('./books.json');

let id = 0;

// Get all the books
router.get('/', (req, res) => {
	res.json(books);
	console.log();
});

// Get a specific book
router.get('/:id', (req, res) => {
	const { id } = req.params;
	res.json(books[id])
});


// Add a new book
router.post('/', (req, res) => {
	const body = req.body;
	books['id' + id] = {
		id: body.id,
		name: body.name
	};
	id++;
	res.json({ message: 'The book has been added' });

});


// Update if book exists or create if doesnt exist
router.put('/:id', (req, res) => {
	const { id } = req.params;
	const body = req.body;
	Object.entries(books).forEach((book, index) => {
		console.log(book);
		if (book[0] === id) {
			books[book[0]] = body;
			console.log("in if");
		}
	});
	res.json({ message: `The book with ID ${id} has been updated` });
});


// Delete a book
router.delete('/:id', (req, res) => {
	const { id } = req.params;
	Object.keys(books).forEach((book, index) => {
		if (book === id) {
			delete books[book];
		}
	});
	res.json({ message: `Book with id #${id} has been deleted` });
});

module.exports = router;