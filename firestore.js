const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');

const serviceAccount = require('./service-account-key.json');

initializeApp({
    credential: cert(serviceAccount),
    databaseURL: "https://alien-program-360918-default-rtdb.firebaseio.com"
});

const db = getFirestore();

module.exports = db